

		<div class="sub-content">
		    <h3>Member</h3>
		    <table class="table table-bordered">
		        <thead>
		            <tr>
		                <th>Username</th>
		                <th>Password</th>
		                <th>Nama</th>
		                <th>Alamat</th>
		                <th>Tanggal Lahir</th>
		                <th>Email</th>
		                <th>No Telepon</th>
		                <th>Action</th>
		                
		            </tr>
		        </thead>
		        <tbody>
		            <?php foreach ($data as $model) : ?>
		            <tr>
		                <td><?php echo $model->username; ?></td>
		                <td><?php echo $model->password; ?></td>
		                <td><?php echo $model->nama; ?></td>
		                <td><?php echo $model->alamat; ?></td>
		                <td><?php  
		                		$date = $model->tanggal; 
		                		$tahun = substr($date,0,4);
		                		$bulan = substr($date,5,2);
		                		$tanggal = substr($date,8,2);
		                		echo $tanggal."/".$bulan."/".$tahun;
		                	?>
		                </td>
		                <td><?php echo $model->email; ?></td>
		                <td><?php echo $model->no_telp; ?></td>
		                
		                <td>
		                    <?php echo CHtml::link(CHtml::encode("Edit"), array('member/editmember', 'username' => $model->username)); ?> |
		                    <?php echo CHtml::link(CHtml::encode("Delete"), array('member/hapusmember', 'username' => $model->username)); ?>
		                </td>
		            </tr>
		        <?php endforeach; ?>
		        </tbody>
		    </table>
		   	<div class="text-right center">
		        <?php echo CHtml::link('Tambah Member', array('member/tambahmember')); ?>
		    </div>

		</div>
		

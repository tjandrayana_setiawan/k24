<?php 
        
    class MemberController extends Controller {

        //public $defaultAction = 'actiontambahmember'; // tulis action y diinginkan
         
        public function actionIndex() {
            $data = Member::model()->findAll();
            $this->render('member', array('data' => $data));
        }


         
        public function actionTambahMember() {
            $modelMember = new Member;
            if (isset($_POST['Member'])) {
                $modelMember->username = $_POST['Member']['username'];    
                $modelMember->password = $_POST['Member']['password'];                    
                $modelMember->nama = $_POST['Member']['nama'];    
                $modelMember->alamat = $_POST['Member']['alamat'];
                $date= $_POST['Member']['tanggal'];
                //echo "<h1>$date</h1>";

                //12/07/2016
                $modelMember->tanggal = $date;
                
                $modelMember->email = $_POST['Member']['email'];
                $modelMember->no_telp   = $_POST['Member']['no_telp'];
                
                try{
                    $modelMember->save();
                    $this->redirect(array('index'));
                }catch(Exception $e){
                    echo '<h1>';
                    echo "Maaf erorr bro";
                    echo '</h1>';
                    //$this->redirect(array('index'));
                }
                
            }
         
            $this->render('tambahmember', array('model' => $modelMember));
        }
         
        public function actionEditMember($username) {
            $modelMember = Member::model()->findByPk($username);
            if (isset($_POST['Member'])) {
                $modelMember->nama = $_POST['Member']['nama'];    
                $modelMember->alamat = $_POST['Member']['alamat'];
                $modelMember->tanggal = $_POST['Member']['tanggal'];
                $modelMember->email = $_POST['Member']['email'];
                $modelMember->no_telp   = $_POST['Member']['no_telp'];
                
                try{
                    $modelMember->save();
                    $this->redirect(array('index'));
                }catch(Exception $e){
                    $this->redirect(array('index'));
                }
                
            }
         
            $this->render('editmember', array('model' => $modelMember));
        }
         
        public function actionHapusMember($username) {
            if (Member::model()->deleteByPk($username)) {
                $this->redirect(array('index'));
            } else {
                throw new CHttpException(404, 'Data gagal dihapus');
            }
        }
    }
 ?>


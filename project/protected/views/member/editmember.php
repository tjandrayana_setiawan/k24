 

	<div class="sub-content">
	    <h2>Edit Member</h2>
	    <?php echo CHtml::beginForm(array('member/editmember', 'username'=>$model->username)); ?>
	 
	 	<div class ="form">
		    <div>
		        <?php echo CHtml::activeLabel($model, 'nama'); ?>
		        <?php echo CHtml::activeTextField($model, "nama", ""); ?>
		    </div>
		 
		 	<div>
		        <?php echo CHtml::activeLabel($model, 'alamat'); ?>
		        <?php echo CHtml::activeTextField($model, "alamat", ""); ?>
		    </div>
		 
		 	<div>
		        <?php echo CHtml::activeLabel($model, 'tanggal'); ?>
		        <?php echo CHtml::activeDateField($model, "tanggal", ""); ?>
		    </div>
		 
		 	<div>
		        <?php echo CHtml::activeLabel($model, 'email'); ?>
		        <?php echo CHtml::activeEmailField($model, "email", ""); ?>
		    </div>
		 
		    <div>
		        <?php echo CHtml::activeLabel($model, 'no_telp'); ?>
		        <?php echo CHtml::activeTextField($model, "no_telp", ""); ?>
		    </div>
		 
		    
		    
		    <div>
		        <?php echo CHtml::submitButton('Submit'); ?>
		        <?php echo CHtml::endForm(); ?>
		    </div>
		</div>
		      
	</div>

 
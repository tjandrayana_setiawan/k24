<?php 
	
	class Member extends CActiveRecord{
		public static function model($className = __CLASS__) {
	        return parent::model($className);
	    }
	 
	    public function tableName() {
	        return 'member';
	    }
	 
	    public function rules() {
	        return array(
	            array('username, password, nama, alamat, tanggal, email, no_telp', 'required', 'message' => '{attribute} tidak boleh kosong.'),
	        );
	    }
	 
	    public function attributeLabels() {
	        return array(
	            'username' => 'Username',
	            'password' => 'Password',
	            'nama' => 'Nama',
	            'alamat' => 'Alamat',
	            'tanggal' => 'Tanggal Lahir',
	          	'email' => 'Email',
	            'no_telp' => 'No Telpon'
	              
	        );
	    }
	}

 ?>